Computer Structure
==================

Slides, examples and assignments for the course "Computer Structure"
of the ETSI Informática de Sevilla.

This is a personal workspace of the author and does not have to be the same
contents used in the oficial courses.

The repository contains the source files. You can browse and dowload auto-generated PDF files using the link below:

* [Browse PDF files](https://gitlab.com/jjchico-edc/edc-units/-/jobs/artifacts/main/browse?job=pdf).

Other course's repos available at [Gitlab](https://gitlab.com/jjchico-edc).

Units
-----

* [Introduction](unit-01-intro/): Introduction to the course.

* [Memories and programmable devices](unit-02-memories_prog_devices/): General
  introduction to memory devices and programmable logic devices.

* [Digital systems design](unit-03-digital_systems/): Digital systems design
  techniques.

* [Design of a (Yet Another) Simple Academic Computer](unit-04-simple_computer/): Design of a simple computer.

* [Study of a real computer: Atmega328P microcontroller](unit-05-avr/): Description and assembly programming of a real microprocessor.

Author
------

Jorge Juan-Chico <jjchico@dte.us.es>

Acknowledgments
---------------

Inspired by the work of many at the Departamento de Tecnología Electrónica,
Universidad de Sevilla.

Licence
-------

CC BY-SA 4.0 [Creative Commons Atribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
